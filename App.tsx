import React from 'react';
import { Provider } from 'react-redux';
import RootStack from './src/nav/RootStack'

import store from './src/reduxs/store'




const App = () => {
  return (
    <Provider store={store} >
      <RootStack />
      </Provider>
    
  )
};



export default App;
