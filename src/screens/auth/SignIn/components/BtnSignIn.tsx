import React from "react";
import { ActivityIndicator, Alert, StyleSheet, TouchableOpacity } from "react-native";
import { Text } from "react-native-ui-lib";
import { Colors } from "react-native/Libraries/NewAppScreen";
import { useDispatch } from "react-redux";
import URL from "../../../../config/Api";
import { IAuth, onSignIn, saveAuthAsync } from "../../../../reduxs/authSlice";

interface Props {
  infoSignIn: {
    email: string;
    password: string;
  };
}

const BtnSignIn = ({ infoSignIn }: Props) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = React.useState<boolean>(false);
  const onPressSignIn = React.useCallback(() => {
      setLoading(true);
    fetch(URL.SignIn, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: infoSignIn.email,
        password: infoSignIn.password,

      }),
    })
      .then((response) => response.json())
      .then((json: IAuth) => {
          const success = json.success;
          if(!success){
              Alert.alert('Thông báo',json.message)
              setLoading(false)
              return;
          }
        dispatch(onSignIn(json));
        setLoading(false)
        saveAuthAsync(json)// save token to async storage
        return json;
      })
      .catch((error) => {
        console.error(error);
      });
  }, [infoSignIn]);
  return (
    <TouchableOpacity style={styles.btnSignIn} onPress={onPressSignIn} disabled={!!loading}>
      {loading ? (
        <ActivityIndicator color={Colors.white} />
      ) : (
        <Text Button_login white>
          Sign In
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default BtnSignIn;

const styles = StyleSheet.create({
  btnSignIn: {
    backgroundColor: Colors.primary,
    justifyContent: "center",
    alignItems: "center",
    height: 48,
    borderRadius: 99,
  },
});
