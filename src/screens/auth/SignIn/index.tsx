import React from "react";
import { StyleSheet, TextInput } from "react-native";
import { Button, Text, View, Colors } from "react-native-ui-lib";
import { FONTS } from "../../../config/Typo";
import BtnSignIn from "./components/BtnSignIn";
const SignIn = () => {
  const [infoSignIn, setInfoSignIn] = React.useState({
    email: "",
    password: "",
  });
  return (
    <View flex bg-white>
      <Text SignUpScreenTittle primary margin-24 marginB-16>
        Connect email address
      </Text>
      <Text marginL-24 marginR-20 b16>
        It's recommended to connect your email{`\n`}address for us to be better
        protect your{`\n`} information
      </Text>
      <View flex center>
        <View marginH-24 marginB-32>
          <Text Onboarding_Text dark80>
            Your Email
          </Text>
          <TextInput
            placeholder="Enter email"
            style={{
              fontSize: 17,
              fontFamily: FONTS.Book,
              color: Colors.dark30,
            }}
            onChangeText={(email: string) =>
              setInfoSignIn((prev) => ({ ...prev, email }))
            }
          />
          <View height={1} bg-dark80 marginT-12 />
        </View>
        <View marginH-24 marginB-32>
          <Text Onboarding_Text dark80 margin-16>
            Set Password
          </Text>
          <TextInput
            placeholder="Enter your password"
            secureTextEntry
            style={{
              fontSize: 17,
              fontFamily: FONTS.Book,
              color: Colors.dark30,
            }}
            onChangeText={(password: string) =>
              setInfoSignIn((prev) => ({ ...prev, password }))
            }
          />
          <View height={1} bg-dark80 marginT-12 />
        </View>
      </View>
      <View flex marginH-24>
        <BtnSignIn infoSignIn={infoSignIn} />
      </View>
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({});
