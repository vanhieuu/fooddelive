import React from "react";
import { StyleSheet, ScrollView, Dimensions } from "react-native";
import { View, Text, Image, Colors, Button } from "react-native-ui-lib";
// import Colors from '../../config/Colors'

const width = Dimensions.get("window").width;

const dataOnboarding = [
  {
    assetName: "onboarding1",
    tittle: "Your Yoga",
    des: "Does Hydroderm Work",
  },
  {
    assetName: "onboarding2",
    tittle: "Your Healthy",
    des: `Recommended You To Use After Before\nBreast Enhancement`,
  },
  {
    assetName: "onboarding3",
    tittle: "Learning to Relax",
    des: "The Health Benefits Of Sunglasses",
  },
];

interface IPageOnboarding {
  assetName: string;
  tittle: string;
  des: string;
  onPress: () => void;
}

const PageOnboarding = ({
  assetName,
  tittle,
  des,
  onPress,
}: IPageOnboarding) => {
  return (
    <View
      style={{ width: width, height: "100%", backgroundColor: Colors.white }}
    >
      <Image
        assetGroup="Onboarding"
        assetName={assetName}
        style={{
          width: width,
          height: (width * 375) / 464,
        }}
      />
      <View flex bottom centerH>
        <Text Onboarding_Tittle marginB-10>
          {tittle}
        </Text>
        <Text Onboarding_Text color={Colors.OnboardingText} marginB-36 center>
          {des}
        </Text>
        <Button
          label="CREAT ACCOUNT"
          color={Colors.primary}
          marginB-30
          style={{ width: width - 82 }}
          onPress={onPress}
        />
      </View>
    </View>
  );
};
interface IRefDots {
  setIndexPageFocus: React.Dispatch<React.SetStateAction<number>>;
}
const Dots = React.forwardRef<IRefDots>((props, ref) => {
  const [indexPageFocus, setIndexPageFocus] = React.useState<number>(0);

  React.useImperativeHandle(ref, () => {
    return {
      setIndexPageFocus,
    };
  });

  const renderDots = React.useCallback(() => {
    let views = [];
    let length = dataOnboarding.length;
    for (let i = 0; i < length; i++) {
      views.push(
        <View
          backgroundColor={indexPageFocus === i ? Colors.primary : Colors.dots}
          style={styles.dots}
          key={i}
        />
      );
    }
    return views;
  }, [indexPageFocus]);
  return <View style={styles.containerDots}>{renderDots()}</View>;
});
const Onboarding = () => {
  const refDots = React.useRef<IRefDots>(null);
  const refScrollView = React.useRef<ScrollView>(null);
  const onMomentumScrollEnd = React.useCallback(({ nativeEvent }) => {
    const x = nativeEvent.contentOffset.x;
    let indexFocus = Math.round(x / width);
    refDots.current?.setIndexPageFocus(indexFocus);
  }, []);
  return (
    <View flex backgroundColor={Colors.white}>
      <ScrollView
        horizontal
        pagingEnabled
        onMomentumScrollEnd={onMomentumScrollEnd}
        showsHorizontalScrollIndicator={false}
        ref={refScrollView}
      >
        {dataOnboarding.map((item, index) => {
          <PageOnboarding
            assetName={item.assetName}
            tittle={item.tittle}
            des={item.des}
            key={item.assetName}
            onPress={() => {
              if (index === 2) return;
              refScrollView.current?.scrollTo({
                x: (index + 1) * width,
                y: 0,
                animated: true,
              });
              refDots.current?.setIndexPageFocus(index + 1);
            }}
          />;
        })}
      </ScrollView>
      <Dots ref={refDots} />
    </View>
  );
};

export default Onboarding;

const styles = StyleSheet.create({
  containerDots: {
    position: "absolute",
    top: (width / 375) * 464,
    height: 10,
    alignSelf: "center",
    flexDirection: "row",
  },
  dots: { borderRadius: 5, marginHorizontal: 5, width: 5, height: 5 },
});
