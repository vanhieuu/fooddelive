import React from "react";
import { StyleSheet, View, Button } from "react-native";
import { Switch } from "react-native-gesture-handler";
import ButtonSubmit from "./components/ButtonSubmit";
import { useSelector } from "react-redux";
import ButtonResetUserInfo from "./components/ButtonResetUserInfo";
import { RootState } from "../../../reduxs/store";
import { IUserState } from "../../../reduxs/userSlice";
import Container from "../../../components/Container";
import Txt from "../../../components/Txt";
import useBoolean from "../../../hook/useBoolean";

const Profile = React.memo(() => {
  const userInfo = useSelector<RootState, IUserState>(
    (state) => state.userInfo
  );
  const [valueSwitch, setValueSwitch] = React.useState<boolean>(false);
  const [showModal, onShowModal, onHideModal] = useBoolean();
  return (
    <Container>
      <Txt>{`Ten:${userInfo.name}`}</Txt>
      <Txt>{`Tuoi:${userInfo.age}`}</Txt>

      <ButtonSubmit userInfo={{ name: userInfo.name, age: userInfo.age }} />
      <ButtonResetUserInfo />
      <Switch value={valueSwitch} onValueChange={setValueSwitch} />
      <Button title="Show Modal" onPress={onShowModal} />
      {!!showModal && (
        <View style={{ width: 100, height: 100, backgroundColor: "red" }}>
          <Button title="Hide Modal" onPress={onHideModal} />
        </View>
      )}
    </Container>
  );
});

export default Profile;

const styles = StyleSheet.create({});
