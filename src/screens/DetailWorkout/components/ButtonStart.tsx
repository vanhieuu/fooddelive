import { NavigationProp, useNavigation } from "@react-navigation/core";
import React from "react";
import { Animated, Dimensions,  StyleSheet, TouchableOpacity } from "react-native";
import { Text, View, Colors } from "react-native-ui-lib";
import { RootStackParamList } from "../../../nav/RootStack";
import { IExerciseItem } from "../../../types/IWorkout";

const widthScreen = Dimensions.get("window").width;
const ButtonAni = Animated.createAnimatedComponent(TouchableOpacity);

interface Props{
  exercises:IExerciseItem[];
  delayNumber:number;
}
const ButtonStart = ({delayNumber,exercises}:Props) => {

  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();


  const scale = React.useRef(new Animated.Value(1)).current;
  const transX = React.useRef(new Animated.Value(-widthScreen)).current;
  const opacity = React.useRef(new Animated.Value(0)).current;

    React.useLayoutEffect(() => {
        Animated.timing(transX,{
            toValue:0,
            duration:700,
            useNativeDriver: true,
            delay: delayNumber*500 
        }).start();
        Animated.timing(opacity,{
            toValue:1,
            duration:700,
            useNativeDriver: true,
        }).start()
    },[])

  const onPressIn = React.useCallback(() => {
    Animated.timing(scale, {
      toValue: 0.95,
      duration: 250,
      useNativeDriver: true,
    }).start();
  }, []);
  const onPressOut = React.useCallback(() => {
    Animated.timing(scale, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    navigate('PlayExercise',{
      exercises:exercises
    })
  }, []);
  return (
    <ButtonAni
      style={{
        transform: [
          { 
            scale: scale,
          },
          {
              translateX:transX
          }
        ],
        opacity:opacity
      }}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      activeOpacity={1}
    >
      <View
        style={{
          position: "absolute",
          bottom: 50,
          width: "40%",
          height: 48,
          alignSelf: "center",
          borderRadius: 99,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: Colors.primary,
        }}
      >
        <Text Onboarding_Text color="#fff">
          Start
        </Text>
      </View>
    </ButtonAni>
  );
};

export default ButtonStart;

const styles = StyleSheet.create({});
