import React from "react";
import { Animated, Easing, StyleSheet, TouchableOpacity } from "react-native";
import { View, Text, Colors } from "react-native-ui-lib";
import { getStatusBarHeight } from "react-native-iphone-x-helper";
import * as Icon from "react-native-iconly";

interface Props {
  scrollY: Animated.Value;
  title: string;
}

const HeaderDetailWorkOut = ({ scrollY,title }: Props) => {

    const opacity = scrollY.interpolate({
        inputRange:[0,120],
        outputRange:[0.1],
    })
    
  return (
    <View
      style={{
        paddingTop: getStatusBarHeight(true),
        paddingBottom: 12,
        flexDirection: "row",
        position: "absolute",
        zIndex: 1,
      }}
    >
      <TouchableOpacity
        style={{
          paddingLeft: 12,
        }}
      >
        <Icon.ArrowLeft size={28} color={"blue"} />
      </TouchableOpacity>
      <Animated.View
        style={{
          backgroundColor: "#fff",
          flex: 1,
          ...StyleSheet.absoluteFillObject,
          zIndex: -1,
          opacity:opacity,
          borderBottomWidth:1,
          borderBottomColor:Colors.grey60,
          paddingTop: getStatusBarHeight(true),
          paddingBottom: 12,
          justifyContent:'center',
          alignItems:'center'
        }}
      >
        <Text Onboarding_Text>{title}</Text>
      </Animated.View>
    </View>
  );
};

export default HeaderDetailWorkOut;

const styles = StyleSheet.create({});
