import { RouteProp, useRoute } from "@react-navigation/core";
import React from "react";
import { StyleSheet, Dimensions, Animated } from "react-native";
import { View, Text, Colors, Assets, Image, Avatar } from "react-native-ui-lib";
import * as Icon from "react-native-iconly";
import dayjs from "dayjs";
import { RootStackParamList } from "../../nav/RootStack";
import ItemExcercise from "./components/ItemExcercise";
import HeaderDetailWorkOut from "./components/HeaderDetailWorkOut";
import { getBottomSpace } from "react-native-iphone-x-helper";
import { getDayTime } from "../../Ultil/handleString";
import ButtonStart from "./components/ButtonStart";
const widthBanner = Dimensions.get("window").width;
const heightBanner = (widthBanner / 1600) * 1000;
const imgBackGrounds = [
  Assets.imgWorkout.imgWorkOut1,
  Assets.imgWorkout.imgWorkOut2,
  Assets.imgWorkout.imgWorkOut3,
  Assets.imgWorkout.imgWorkOut4,
  Assets.imgWorkout.imgWorkOut5,
  Assets.imgWorkout.imgWorkOut6,
  Assets.imgWorkout.imgWorkOut7,
  Assets.imgWorkout.imgWorkOut8,
  Assets.imgWorkout.imgWorkOut9,
  Assets.imgWorkout.imgWorkOut10,
];

const DetailWorkout = () => {
  const route = useRoute<RouteProp<RootStackParamList, "DetailWorkout">>();
  const workout = route.params.item;

  const scrollY = React.useRef(new Animated.Value(0)).current;

  return (
    <View style={{ flex: 1, backgroundColor: Colors.white }}>
      <HeaderDetailWorkOut scrollY={scrollY} title={workout.name} />
      <Animated.ScrollView
        onScroll={Animated.event(
          // scrollX = e.nativeEvent.contentOffset.x
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: scrollY,
                },
              },
            },
          ],
          {
            useNativeDriver: true,
          }
        )}
        contentContainerStyle={{
          paddingBottom: getBottomSpace(),
        }}
      >
        <View
          style={{
            height: heightBanner,
            width: widthBanner,
          }}
        >
          <Image
            source={imgBackGrounds[workout.muscle_part_id % 10]}
            style={{
              width: widthBanner,
              height: heightBanner,
            }}
            overlayType={Image.overlayTypes.BOTTOM}
          />
        </View>
        <Text h28 marginR-16 marginV-12>
          {workout.name}
        </Text>
        <View row centerV marginH-16 paddingV-4>
          <Icon.Message />
          <Text marginH-16>{workout.commentCount} commnet</Text>
        </View>
        <View row centerV marginH-16 paddingV-4>
          <Icon.Calendar color={Colors.grey10} />
          <Text marginL-12>
            Ngày tạo:{dayjs(workout.created_at).format("MM/DD/YY")}
          </Text>
        </View>
        <View row centerV marginH-16 paddingV-4 marginB-12>
          <Icon.Calendar color={Colors.grey10} />
          <Text marginL-12>
            Calo tiêu thụ:{parseInt(workout.calo, 10)} calos
          </Text>
        </View>
        <Text Content marginH-16 marginV-4>
          {" "}
          Danh sách bài tập
        </Text>
        {workout.exercise_items.map((item,index) => {
          <ItemExcercise item={item.exercise} key={item.id} index={index} />;
        })}
        <Text Content marginH-16 marginV-4>
          Bình luận({workout.exercise_items.length})
        </Text>
        {workout.comments.map((item) => {
          return (
            <View
              row
              marginH-18
              marginV-8
              style={{
                borderBottomWidth: 1,
                borderColor: Colors.grey60,
              }}
              key={item.id}
            >
              <Avatar size={40} />
              <View marginL-12 flex>
                <Text welcome> Customer_id:{item.customer_id}</Text>
                <Text TabBarBottom> {getDayTime(item.created_at)}</Text>
                <Text Exercise marginV-8 flex >
                  {item.content}
                </Text>
              </View>
            </View>
          );
        })}
      </Animated.ScrollView>
      <ButtonStart delayNumber={workout.exercise_items.length} exercises={workout.exercise_items} />
    </View>
  );
};

export default DetailWorkout;

const styles = StyleSheet.create({});
