import React from 'react';
import {StyleSheet} from 'react-native';
import {View, Text, Colors, Image} from 'react-native-ui-lib';
import {IExerciseItem} from '../../../types/IWorkout';
const NextExercise = ({exerciseNext}: {exerciseNext: IExerciseItem}) => {
  return (
    <React.Fragment>
      <Text m16 margin-16 color={Colors.white}>
        Next
      </Text>
      <View row marginH-16>
        <Image
          source={{uri: exerciseNext.exercise.img}}
          style={{
            width: 60,
            height: 60,
          }}
        />
        <View marginL-16>
          <Text h16 color={Colors.white}>
            {exerciseNext.exercise.name}
          </Text>
          <Text
            b10
            color={
              Colors.white
            }>{`${exerciseNext.time} second rest: ${exerciseNext.rest} second`}</Text>
        </View>
      </View>
    </React.Fragment>
  );
};

export default NextExercise;

const styles = StyleSheet.create({});
