import React from 'react';
import {StyleSheet, Animated} from 'react-native';
import {View, Text, Colors} from 'react-native-ui-lib';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';

interface Props {
  duration: number;
  onComplete: () => void;
  status: 'Duration' | 'Rest' |'Finish';
}

const CountDownExercise = ({onComplete, duration, status}: Props) => {
  const [isPlaying, setIsPlaying] = React.useState(false);
  const transX = React.useRef(new Animated.Value(30)).current;
  const opacity = React.useRef(new Animated.Value(0)).current;

  React.useEffect(() => {
    Animated.timing(transX, {
      toValue: 0,
      duration: 250,
      useNativeDriver: true,
      delay: 300,
    }).start();
    Animated.timing(opacity, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true,
      delay: 300,
    }).start(({finished}) => {
      if (finished) {
        setIsPlaying(true);
      }
    });
  }, []);

  return (
    <Animated.View
      style={{
        transform: [{translateX: transX}],
        opacity,
      }}>
      <Text m17 center color={Colors.white} marginT-24 marginB-8>
        {status}
      </Text>
      <CountdownCircleTimer
        size={60}
        strokeWidth={4}
        isPlaying={isPlaying}
        duration={duration}
        colors={'#F7B801'}
        onComplete={(totalElapsedTime: number) => {
          setIsPlaying(false);
          onComplete();
        }}>
        {({remainingTime, animatedColor}) => {
          return (
            <>
              <Animated.Text style={{color: animatedColor, fontSize: 24}}>
                {remainingTime}
              </Animated.Text>
            </>
          );
        }}
      </CountdownCircleTimer>
    </Animated.View>
  );
};

export default CountDownExercise;

const styles = StyleSheet.create({});
