import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {View, Text, Colors} from 'react-native-ui-lib';
import Video from 'react-native-video';
import {IExerciseItem} from '../../../types/IWorkout';
const widthScreen = Dimensions.get('window').width;

const ItemPlayExercise = ({
  item,
  play,
}: {
  item: IExerciseItem;
  play: boolean;
}) => {
  return (
    <View flex width={widthScreen}>
      <Text m28 center color={Colors.white} marginV-12>
        {item.exercise.name}
      </Text>
      <View marginH-16>
        <Video
          source={{uri: item.exercise.video}}
          style={{
            width: '100%',
            height: 200,
            backgroundColor: Colors.dark20,
          }}
          repeat={true}
          paused={!play}
        />
      </View>
    </View>
  );
};

export default ItemPlayExercise;

const styles = StyleSheet.create({});
