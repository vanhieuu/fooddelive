import { RouteProp, useRoute } from "@react-navigation/core";
import React from "react";
import { FlatList, StyleSheet } from "react-native";
import { Button, Colors, View } from "react-native-ui-lib";
import { RootStackParamList } from "../../nav/RootStack";
import { IExerciseItem } from "../../types/IWorkout";

import { getBottomSpace } from "react-native-iphone-x-helper";
import ItemPlayExercise from "./components/ItemPlayExercise";
import CountDownExercise from "./components/CountDownExercise";
import NextExercise from "./components/NextExcersise";

const PlayExercise = () => {
  const route = useRoute<RouteProp<RootStackParamList, "PlayExercise">>();
  const exercise = route.params.exercises;
  const [currentIndex, setCurrentIndex] = React.useState(0);
  const [status, setStatus] = React.useState<"Duration" | "Rest" | "Finish">(
    "Duration"
  );

  const exerciseNext = React.useMemo(() => {
    if (currentIndex === exercise.length - 1) return null;
    return exercise[currentIndex + 1];
  }, [currentIndex, exercise]);

  const exerciseCurrent = React.useMemo(() => {
    return exercise[currentIndex];
  }, [currentIndex, exercise]);

  const isExerciseLast = React.useMemo(
    () => currentIndex === exercise.length - 1,
    [currentIndex, exercise]
  );

  const refFlatlist = React.useRef<FlatList>(null);

  const renderItem = React.useCallback(
    ({ item, index }: { item: IExerciseItem; index: number }) => {
      return <ItemPlayExercise item={item} play={index === currentIndex} />;
    },
    [currentIndex]
  );

  return (
    <View flex backgroundColor={Colors.dark10}>
      <View height={280}>
        <FlatList
          data={exercise}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          horizontal
          pagingEnabled
          style={{}}
          scrollEnabled={false}
          ref={refFlatlist}
        />
      </View>

      <View centerH marginV-16>
        {status === "Duration" ? (
          <CountDownDuration
            time={3}
            onComplete={() => {
              if (status === "Duration") {
                setStatus("Rest");
              }
            }}
            status={status}
          />
        ) : (
          <CountDownExercise
            duration={3}
            onComplete={() => {
              if (isExerciseLast) {
                setStatus("Finish");
                return;
              }

              setCurrentIndex((prev) => {
                return prev + 1;
              });
              setStatus("Duration");
              refFlatlist.current?.scrollToIndex({ index: currentIndex + 1 });
            }}
            status={status}
          />
        )}
      </View>

      {!!exerciseNext && <NextExercise exerciseNext={exerciseCurrent} />}

      <View flex />
      <View
        marginH-16
        style={{
          marginBottom: getBottomSpace() || 16,
        }}
      >
        <Button label="Next Exercise" />
      </View>
    </View>
  );
};

export default PlayExercise;
const CountDownDuration = ({
  time,
  onComplete,
  status,
}: {
  time: number;
  onComplete: () => void;
  status: "Duration" | "Rest";
}) => {
  return (
    <CountDownExercise
      duration={time}
      onComplete={onComplete}
      status={status}
    />
  );
};

const styles = StyleSheet.create({});
