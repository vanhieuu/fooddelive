import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Onboarding from "../screens/Onboarding";
import SignIn from "../screens/auth/SignIn";
import MainTab from "./MainTab";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../reduxs/store";
import {
  EStatusAuth,
  getAuthSync,
  IAuth,
  onSignIn,
  updateStatusAuth,
} from "../reduxs/authSlice";
import { Colors, View } from "react-native-ui-lib";
import { ActivityIndicator, Alert } from "react-native";

import URL from "../config/Api";
import DetailWorkout from "../screens/DetailWorkout";
import { IExerciseItem, IWorkout } from "../types/IWorkout";
import playExercise from "../screens/playExercise";

export type RootStackParamList = {
  ProWelcomefile: undefined;
  Onboarding: undefined;
  CreatAccount: undefined;
  SignUp: undefined;
  MainTab: undefined;
  Setting: undefined;
  CategoryDetail: undefined;
  CategoryDetailSub: undefined;
  TopicDetail: undefined;
  ProfileAddFriend: undefined;
  ProfileCouponsVouchers: undefined;
  MyTopic: undefined;
  Search: undefined;
  FAQ: undefined;
  Filter: undefined;
  SignIn: undefined;
  DetailWorkout: {
    item: IWorkout;
  };
  PlayExercise: {
    exercises: IExerciseItem[];
  };
};

const Stack = createNativeStackNavigator<RootStackParamList>();

const RootStack = () => {
  const isSignIn = useSelector<RootState, EStatusAuth>(
    (state) => state.auth.statusAuth
  );
  const dispatch = useDispatch();
  const checkLogin = React.useCallback(async () => {
    const auth: IAuth | null = await getAuthSync();
    if (auth) {
      //call api validate Auth
      fetch(URL.ValidateToken, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          token: auth.token,
        }),
      })
        .then((response) => response.json())
        .then((json: { success: boolean; message: string }) => {
          const success = json.success;
          //token fail
          if (!success) {
            Alert.alert("Thông báo", json.message);
            dispatch(updateStatusAuth({ statusAuth: EStatusAuth.unauth }));
            return;
          }
          //token success
          dispatch(onSignIn(auth));
          return json;
        });
    } else {
      dispatch(updateStatusAuth({ statusAuth: EStatusAuth.unauth }));
    }
  }, []);
  React.useEffect(() => {
    checkLogin();
  }, []);

  if (isSignIn === EStatusAuth.check) {
    return (
      <View flex center>
        <ActivityIndicator color={Colors.primary} />
      </View>
    );
  }
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SignIn">
        {isSignIn === EStatusAuth.unauth ? (
          <>
            <Stack.Screen
              name="Onboarding"
              component={Onboarding}
              options={{ headerShown: false }}
            />
            <Stack.Screen name="SignIn" component={SignIn} />
          </>
        ) : (
          <>
            <Stack.Screen
              name="MainTab"
              component={MainTab}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="DetailWorkout"
              component={DetailWorkout}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="PlayExercise"
              component={playExercise}
              options={{ headerStyle: 
                { 
                  backgroundColor: Colors.dark10
                 },
                 headerTintColor:'#ffff',
                 title:'',
                 headerBackTitleVisible:false,
             }}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootStack;
