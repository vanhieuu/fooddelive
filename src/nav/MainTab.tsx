import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Training from "../screens/mainTab/Training";
import HealthyTips from "../screens/mainTab/HealthyTips";
import News from "../screens/mainTab/News";
import Goals from "../screens/mainTab/Goals";
import Profile from "../screens/mainTab/Profile";
import { Assets, Colors, Image, View, Button } from "react-native-ui-lib";
import { BottomTabHeaderProps } from "@react-navigation/bottom-tabs/lib/typescript/src/types";
import { Header } from "@react-navigation/elements";
import { FONTS } from "../config/Typo";

export type MainTabParamList = {
  News: undefined;
  Training: undefined;
  HealthyTips: undefined;
  Goals: undefined;
  Profile: undefined;
};

const Tab = createBottomTabNavigator();

const MainTab = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: Colors.primary,
        tabBarInactiveTintColor: Colors.dark50,
      }}
    >
      <Tab.Screen
        name="News"
        component={News}
        options={{
          tabBarIcon: ({ color }) => (
            <Image
              assetGroup="icons"
              assetName="icon_tab_new"
              tintColor={color}
            />
          ),
          tabBarLabel: "News",
          headerTransparent: true,
          header: (props: BottomTabHeaderProps) => (
            <Header
              title="Exercises"
              headerTitleStyle={{
                fontSize: 27,
                fontFamily: FONTS.Heavy,
              }}
              headerTitleAlign="left"
              headerRight={({ tintColor, pressColor, pressOpacity }) => {
                return (
                  <View row>
                    <Button
                      iconSource={Assets.iconHeader.icSearch}
                      style={{ width: 44, height: 44 }}
                      link
                      color={tintColor}
                    />
                    <Button
                      iconSource={Assets.iconHeader.icOption}
                      style={{ width: 44, height: 44 }}
                      link
                      color={tintColor}
                    />
                  </View>
                );
              }}
              headerStyle={{
                backgroundColor: Colors.transparent,
              }}
              headerTintColor={Colors.white}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Training"
        component={Training}
        options={{
          tabBarIcon: ({ color }) => (
            <Image
              assetGroup="icons"
              assetName="icon_tab_play"
              tintColor={color}
            />
          ),
          tabBarLabel: "Training",
        }}
      />
      <Tab.Screen
        name="HealthyTips"
        component={HealthyTips}
        options={{
          tabBarIcon: ({ color }) => (
            <Image assetGroup="icons" assetName="notebook" tintColor={color} />
          ),
          tabBarLabel: "HealthyTips",
        }}
      />
      <Tab.Screen
        name="Goals"
        component={Goals}
        options={{
          tabBarIcon: ({ color }) => (
            <Image
              assetGroup="icons"
              assetName="icon_tab_goal"
              tintColor={color}
            />
          ),
          tabBarLabel: "Goals",
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ color }) => (
            <Image
              assetGroup="icons"
              assetName="icon_tab_profile"
              tintColor={color}
            />
          ),
          tabBarLabel: "Profile",
        }}
      />
    </Tab.Navigator>
  );
};
export default MainTab;
