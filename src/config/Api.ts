const MAIN_URL = 'http://saq.herokuapp.com/api';

const URL = {
  SignIn: MAIN_URL + `/login`,
  ValidateToken: MAIN_URL + `/validate`,
  exercises: MAIN_URL + `/exercises`,
  workouts: (user_id: number) => MAIN_URL + `/workouts?user_id=${user_id}`,
};
export default URL;
