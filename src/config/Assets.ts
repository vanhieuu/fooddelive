
import {Assets} from 'react-native-ui-lib';
Assets.loadAssetsGroup('icons', {
    notebook:require('../assets/Notebook.png'),
    icon_tab_goal:require('../assets/icon_tab_goal.png'),
    icon_tab_new:require('../assets/icon_tab_new.png'),
    icon_tab_profile:require('../assets/icon_tab_profile.png'),
    icon_tab_play:require('../assets/icon_tab_play.png')
  });
  Assets.loadAssetsGroup('onboarding', {
    onboarding1: require('../assets/onboarding1.png'),
    onboarding2:require('../assets/onboarding2.png'),
    onboarding3:require('../assets/onboarding3.png'),
});
  Assets.loadAssetsGroup('illustrations',{
    CreatAccount:require('../assets/CreateAccountImg.png'),
    Illustrations:require('../assets/illustrations.png')
  })
  Assets.loadAssetsGroup('imgBackground',{
    bgNew: require('../assets/ImgScreen/img_new_bg.png')
  })
  Assets.loadAssetsGroup('iconHeader',{
    icSearch:require('../assets/icon_header_search.png'),
    icOption:require('../assets/icon_header_option.png')
  })
  Assets.loadAssetsGroup('imgWorkout', {
    imgWorkout1: require('../assets/home_page_section_banner.jpeg'),
    imgWorkout2: require('../assets/popular_banner.jpeg'),
    imgWorkout3: require('../assets/profile_banner.jpeg'),
    imgWorkout4: require('../assets/saq_banner_1920x850.jpeg'),
    imgWorkout5: require('../assets/wk_bg1.jpeg'),
    imgWorkout6: require('../assets/wk_bg2.jpeg'),
    imgWorkout7: require('../assets/wk_bg3.jpeg'),
    imgWorkout8: require('../assets/wk_bg4.jpeg'),
    imgWorkout9: require('../assets/workout_banner_1.jpeg'),
    imgWorkout10: require('../assets/workout_banner_2.jpeg'),
  });
  
export default Assets
