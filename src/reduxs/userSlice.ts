import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface IUserState {
  name: string;
  age: string;
  address: string;
}

const initValue: IUserState = {
  name: 'Thân Văn Hiếu',
  age: '22',
  address: 'Lạng Sơn',
};

export const userInfoSlice = createSlice({
  name: 'userInfo',
  initialState: initValue,
  reducers: {
    onUpdateUserInfo: (state, action: PayloadAction<IUserState>) => {
      state.name = action.payload.name;
      state.age = action.payload.age;
      state.address = action.payload.address;
    },
    onResetUserInfo:state =>{
      state.name ='Thân Văn Hiếu';
      (state.age='22'),(state.address='Lạng Sơn')
    }
  },
});

// Action creators are generated for each case reducer function
export const {onUpdateUserInfo,onResetUserInfo} = userInfoSlice.actions;

export default userInfoSlice.reducer;
